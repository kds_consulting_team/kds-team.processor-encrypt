# Encrypt processor

This processor allows you to encrypt input files using specified encryption methods.
Currently, the processor has the following encryption methods : RSA
  

## Configuration
 - encryption_method - [REQ]
 - \#public_key - [REQ] Whole private key with -----BEGIN RSA PUBLIC KEY----- and -----END RSA PUBLIC KEY----- with all new lines separated by \n (one slash)


## Sample configuration
```json
{
 "definition": {
          "component": "kds-team.processor-encrypt"
        },
  "parameters": {
    "encryption_method": "RSA",
    "#public_key": "-----BEGIN RSA PUBLIC KEY-----\nfake_key\nfake_key\n-----END RSA PUBLIC KEY-----"
  }
}
```
