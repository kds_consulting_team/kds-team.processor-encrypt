'''
Created on 12. 11. 2018

@author: esner
'''
import unittest
import mock
import os
from freezegun import freeze_time

from component import Component
from encryptor.encryptor_factory import EncryptorFactory

from keboola.component.base import ComponentBase, UserException


class TestComponent(unittest.TestCase):

    # set global time to 2010-10-10 - affects functions like datetime.now()
    @freeze_time("2010-10-10")
    # set KBC_DATADIR env to non-existing dir
    @mock.patch.dict(os.environ, {'KBC_DATADIR': './non-existing-dir'})
    def test_run_no_cfg_fails(self):
        with self.assertRaises(ValueError):
            comp = Component()
            comp.run()

    def test_RSA(self):
        private_key = '-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAp10mmT6OWr7u9l7UTGGCS6rzh7uZ74GR9duGflum0cJVL' \
                      'tpT\nRlMYo8xsV9nvXfqH8odpjSYW0fX/t1x9pzj0KZhZEX8sr1x/fy654JZPHhCD2YTO\nlMHvaaDlWAgeyDFggz6' \
                      'farO1utMi7/IQxNW2rHLCmS9Hn7BfJ7WjVjfZJsY0wWi1\neUV7t/rVCKuyCXMptyXExpWMrM1bCkNV7Pw0BZ6p/wXRe' \
                      'IfXxyc4mN3ciCLQ5Ya5\ntTi51cM70rkHTf6BQGNxmyd8tKwNRLuZCkHd9QGOqwZthe8GEBM3ZK0D1MugIqMy\nXQj2' \
                      '193OcOQY8vmA8fpTyUL4zJsLdGKrd9TvfQIDAQABAoIBAAiojosmC9pQ/27a\niBQthT6h6PhwMa4M336/LS2TU0WNzr' \
                      'deTymXufIbbM18tl//ypG3BLRrMIoLNoAH\ndm60yRjvy0fW8vpdiDENu0sNq6ksBI43yzIx+E9YJhVQUjtcJpiToE62X' \
                      'mP8cVXF\nia2NOuagB0ZChmuSBdI5mOooeIEDcZ6+3Hy+wYCSg+zoEJeOx02tdHugBf2rSrgy\nhx5HMhbnL4ghVZbRpxT' \
                      'SYWgLWN/3xQQZi0VeOuD9vaGJrmQW+veQ3j+XGfJ4C+Il\nkJhidE2Jcrp5RLrQzjqHXetnve/5OuD7ic10R7L7+b' \
                      'maTgESCERo4ce6QqzlYpjv\nffk7SRECgYEAxpwUXTOGUUmmRWkCtqqQF3uiYhB2f4YABn1XW1etHBlRoOrO2e' \
                      'Bn\nJGRDW+Nb+XdXNGmvDBUwx1L7hltOi2vHLFBiStas0ZnCYPS/hhPKiApKghwHdTdq\n+bFBB9/gV6eXImSvq2p' \
                      'bk0wU3m6XCctvGYgPteC7P99BEfPepfm9yFUCgYEA17my\nyLqvjiX+v+pChYkkUW78+XOGfA3Th7S7Z72RNUl9vb0hN' \
                      'IPpcEOiRMxnbbT4YxJu\nz7jo7w8Fk3fYbLlz/u8JDkTst0s8jROVohYjBSCH/UZSk5OZrVwYfBkd9zZdxA8W\ndudYZ' \
                      'XOShTN9hn/qpb9ggPGgHirr2ddW/VIz0okCgYB0Gros4fobexqgaQsVDhC+\nKaB8A3PCiOtq38s5sr+YNZTqpVnXNMAX' \
                      'HiXiib8yzvlDwqR6Q8kIPRKdMLNx/cep\nuDQ929Gblm9zjqCIGPnFHX8BILGbbJI09dI2cEIN/AL2MDYh10NBkLCCd9QD' \
                      '/dEs\n88V7Za0RYcoAXLhD/wNbIQKBgATmujInU6/GGzifCO3hxMp43BSK1GZjJggySgO8\nYd01Ez9JWcTIrngXgUtQ3' \
                      'ZS3hOEzXQMpL1mnHLhSA7/Mot8K7Ui+tiYf9ns6E69A\nE4GdUJmnjF6//sDak8/V/kFMoeq/cQI4lWCQtKaEGfkBVQJ' \
                      '5C6dCViH5Vax4YQiW\nYokJAoGBAJSCS+c5sU92+FwqntrcDtC4w68DiyxbS1KQuvDM+7oz4gLQCShhUPAS\nUzeJ8z8' \
                      '7E34Y5DF/gXx08zVXyzStQZif5GQYkIrIgmc+vYcF24XBAPM9LPYFh+Nb\nLgUMqoRivjm2ELNUtgt1KA5TRHp1N+Wor' \
                      'zZNSkBYQxG+2RrVrvwz\n-----END RSA PRIVATE KEY-----'
        public_key = "-----BEGIN RSA PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp10mmT6OWr7u9l7" \
                     "UTGGC\nS6rzh7uZ74GR9duGflum0cJVLtpTRlMYo8xsV9nvXfqH8odpjSYW0fX/t1x9pzj0\nKZhZEX8sr1x/fy654J" \
                     "ZPHhCD2YTOlMHvaaDlWAgeyDFggz6farO1utMi7/IQxNW2\nrHLCmS9Hn7BfJ7WjVjfZJsY0wWi1eUV7t/rVCKuyCXMpty" \
                     "XExpWMrM1bCkNV7Pw0\nBZ6p/wXReIfXxyc4mN3ciCLQ5Ya5tTi51cM70rkHTf6BQGNxmyd8tKwNRLuZCkHd\n9QGOqwZt" \
                     "he8GEBM3ZK0D1MugIqMyXQj2193OcOQY8vmA8fpTyUL4zJsLdGKrd9Tv\nfQIDAQAB\n-----END RSA PUBLIC KEY-----"

        message = "hello world"
        message_bytes = str.encode(message)
        encryptor = EncryptorFactory.get_encryptor("RSA")
        encrypted_data = encryptor.encrypt(message_bytes, public_key)
        decrypted_data = encryptor.decrypt(encrypted_data, private_key)
        decrypted_data = decrypted_data.decode("utf-8")

        self.assertEqual(decrypted_data, message)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
