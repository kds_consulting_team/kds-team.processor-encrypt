from enum import Enum
from encryptor.rsa_encryptor import RSAEncryptor


class EncryptorFactory:
    class EncryptorTypes(Enum):
        RSA = "RSA"

    @staticmethod
    def get_encryptor(encryptor_type):
        if encryptor_type == EncryptorFactory.EncryptorTypes.RSA.value:
            return RSAEncryptor
