import abc


class Encryptor(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def encrypt(self, data):
        pass
