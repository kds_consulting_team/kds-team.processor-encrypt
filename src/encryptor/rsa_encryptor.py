from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from encryptor.encryptor import Encryptor


class RSAEncryptor(Encryptor):
    @staticmethod
    def encrypt(data, public_key_text):
        rsa_public_key = RSA.importKey(public_key_text)
        rsa_public_key = PKCS1_OAEP.new(rsa_public_key)
        return rsa_public_key.encrypt(data)

    @staticmethod
    def decrypt(encrypted_data, private_key_text):
        rsa_private_key = RSA.importKey(private_key_text)
        rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
        return rsa_private_key.decrypt(encrypted_data)
