import os
import logging
from encryptor.encryptor_factory import EncryptorFactory

from keboola.component.base import ComponentBase, UserException

# configuration variables
KEY_ENCRYPTION_METHOD = 'encryption_method'
KEY_PUBLIC_KEY = '#public_key'

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_ENCRYPTION_METHOD, KEY_PUBLIC_KEY]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self):
        params = self.configuration.parameters
        encryption_method = params.get(KEY_ENCRYPTION_METHOD)
        public_key = params.get(KEY_PUBLIC_KEY)
        encryptor = EncryptorFactory.get_encryptor(encryption_method)

        in_files = self.get_input_files_definitions()

        for file in in_files:
            logging.info(f"Encrypting file {file.name} using {encryption_method}")
            self.encrypt_file(file, encryptor, public_key)

    def encrypt_file(self, file, encryptor, public_key):
        out_file_destination = self.files_out_path
        with open(file.full_path, 'rb') as in_file:
            data = in_file.read()
            encrypted_data = self.encrypt_date(data, encryptor, public_key)
        out_file_path = os.path.join(out_file_destination, file.name)
        with open(out_file_path, 'wb') as out_file:
            out_file.write(encrypted_data)

    @staticmethod
    def encrypt_date(data, encryptor, public_key):
        try:
            return encryptor.encrypt(data, public_key)
        except ValueError as value_error:
            raise UserException(value_error) from value_error


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
